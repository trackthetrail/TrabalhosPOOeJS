export class Vila{
    private nome: string;
    private lider: string;
    private populacao: number;
    

    constructor(nome: string, lider: string, populacao: number){
        this.nome = nome;
        this.lider = lider;
        this.populacao = populacao;
    }
