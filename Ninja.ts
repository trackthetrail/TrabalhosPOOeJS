import { Vila } from './Vila';

export class Ninja{
    private nome: string;
    private vila: Vila;
    private liberacao: string;
    private missoesFeitas: number;

constructor(nome: string, vila: Vila, liberacao: string, missoesFeitas: number){
    this.nome = nome;
    this.vila = vila;
    this.liberacao = liberacao;
    this.missoesFeitas = missoesFeitas;
}
public setNome(nome: string){
    this.nome = nome;
}


public descobreliberacao(): string{
    let a = Math.floor(Math.random()*5);
    if (a == 1){
        return "fogo"
    }
    if (a == 2){
        return "ar"
    }
    if (a == 3){
        return "pedra"
    }
    if (a == 4){
        return "agua"
    }
    if (a == 5){
        return "raio"
    }
}
}
let x = new Vila("Konoha", "Hokage", 1000);

let z = new Ninja("Pedro",x,"liberacao", 72)

console.log(z.descobreliberacao())